//+------------------------------------------------------------------+
//|                                                        iTest.mq4 |
//|                                                           JWiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "JWiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Classes/Trender.mqh>

#property indicator_separate_window
#property indicator_minimum -100
#property indicator_maximum 100
#property indicator_buffers 1
#property indicator_plots   1

#property indicator_label1  "Trend"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrWhite
#property indicator_style1  STYLE_SOLID
#property indicator_width1  10

input int TF, rsiPeriods, adxPeriods;

double trendBuffer[];
Trender tr1(TF,rsiPeriods,adxPeriods);

int OnInit(){
    SetIndexBuffer(0,trendBuffer);
    return(INIT_SUCCEEDED);
}

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]){
    int i, pos;
    
    

    ArraySetAsSeries(trendBuffer,true);

    if(prev_calculated<1){
        trendBuffer[0] = EMPTY_VALUE;
    }
    if(prev_calculated>1){
        pos = prev_calculated - 1;
    }
    else{
        pos = 0;
    }

    for(i=pos;i<rates_total;i++){
        tr1.Calculate(i);
        if(tr1.trendUp){trendBuffer[i] = tr1.trendStrength;}
        else if(tr1.trendDown){trendBuffer[i] = -tr1.trendStrength;}
        else{trendBuffer[i] = 0;}
    }
    return rates_total;
}