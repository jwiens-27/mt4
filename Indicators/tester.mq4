//+------------------------------------------------------------------+
//|                                                       tester.mq4 |
//|                                     Copyright 2019, Joshua Wiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_separate_window
#property indicator_minimum -3
#property indicator_maximum 3
#property indicator_buffers 1
#property indicator_plots   1

#include <my-processors.mqh>
//--- plot trend
#property indicator_label1  "trend"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrRed
#property indicator_width1  10
//--- indicator buffers
double         trendBuffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
Phaser p1;
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,trendBuffer);
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
  int i, pos;
  if(rates_total<=14){return(0);}
  ArraySetAsSeries(trendBuffer,false);
  Print(rates_total);
  if(prev_calculated<1){
      for(i=0; i<14; i++){
        trendBuffer[i]=EMPTY_VALUE;
      }
  }

  if(prev_calculated>1){pos=prev_calculated-1;}    
  else{pos=0;}
  for(i=pos; i<rates_total && !IsStopped(); i++){
    p1.GetTrend(0,i);
    trendBuffer[i]=p1.trendStrength*p1.trendDirection;
    Print("trend strenth = ",p1.trendStrength,", isTrending = ",p1.isTrending);
    //Print("SMA[",i,"] = ",SMABuffer[i]);
    // if(i == prev_calculated-1&&TimeCurrent()-time[i]!=0){
    //    //Print("open time = ",time[i]," current time = ",TimeCurrent());
    //    ROCBuffer[i]=1000000*(SMABuffer[i]-SMABuffer[i-1])/(TimeCurrent()-time[i-1]);
    //    ROCDiffBuffer[i]=ROCBuffer[i]-ROCBuffer[i-1];
    // }
    // else if(i!=0&&i!=1){
    //    ROCBuffer[i]=1000000*(SMABuffer[i]-SMABuffer[i-1])/(time[i]-time[i-2]);
    //    if(i!=2){ROCDiffBuffer[i]=ROCBuffer[i]-ROCBuffer[i-1];}
    //    else{ROCDiffBuffer[i]=EMPTY_VALUE;}
    // }
    // else{ROCBuffer[i]=EMPTY_VALUE;}
    //Print("SMA[",i,"] = ",SMABuffer[i]," ROC[",i,"] = ",ROCBuffer[i]);
    //---
     }
      
//--- return value of prev_calculated for next call
  return(rates_total);
  }
//+------------------------------------------------------------------+
