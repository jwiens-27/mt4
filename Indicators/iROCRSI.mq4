//+------------------------------------------------------------------+
//|                                                        iTest.mq4 |
//|                                                           JWiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "JWiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <MovingAverages.mqh>

#property indicator_separate_window
#property indicator_minimum -.25
#property indicator_maximum .25
#property indicator_buffers 3
#property indicator_plots   2
//--- plot ROC
#property indicator_label1  "ROC"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrWhite
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot ROC Diff
#property indicator_label2  "ROC Diff"
#property indicator_type2   DRAW_HISTOGRAM
#property indicator_color2  clrDarkCyan
#property indicator_style2  STYLE_SOLID
#property indicator_width2  10
//--- plot SMA
#property indicator_label3  "RSI"
#property indicator_type3   DRAW_NONE
//#property indicator_color1  clrRed
//#property indicator_style1  STYLE_SOLID
//#property indicator_width1  1


//--- input parameters
input int      periods;
//--- indicator buffers
double         ROCBuffer[];
double         ROCDiffBuffer[];
double         RSIBuffer[];
//-- utility variables
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,ROCBuffer);
   SetIndexBuffer(1,ROCDiffBuffer);
   SetIndexBuffer(2,RSIBuffer);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]){
//---
   int i,pos;
//---
   if(rates_total<=periods || periods<=0)
      return(0);
//--- counting from 0 to rates_total
   ArraySetAsSeries(ROCBuffer,false);
   ArraySetAsSeries(ROCDiffBuffer,false);
   ArraySetAsSeries(RSIBuffer,false);
   ArraySetAsSeries(close,false);
   ArraySetAsSeries(time,false);
//--- initial zero
   if(prev_calculated<1)
     {
      for(i=0; i<periods; i++)
        {
         ROCBuffer[i]=EMPTY_VALUE;
         ROCDiffBuffer[i]=EMPTY_VALUE;
         RSIBuffer[i]=EMPTY_VALUE;
        }
     }
//--- starting calculation
   if(prev_calculated>1)
      pos=prev_calculated-1;
   else
      pos=0;
//--- main cycle
   for(i=pos; i<rates_total && !IsStopped(); i++)
     {
      //--- middle line
      RSIBuffer[i]=iRSI(NULL,0,periods,0,i);
      //Print("SMA[",i,"] = ",SMABuffer[i]);
      if(i == prev_calculated-1&&TimeCurrent()-time[i]!=0){
         //Print("open time = ",time[i]," current time = ",TimeCurrent());
         ROCBuffer[i]=100*(RSIBuffer[i]-RSIBuffer[i-1])/(TimeCurrent()-time[i-1]);
         ROCDiffBuffer[i]=ROCBuffer[i]-ROCBuffer[i-1];
      }
      else if(i!=0&&i!=1){
         ROCBuffer[i]=100*(RSIBuffer[i]-RSIBuffer[i-1])/(time[i]-time[i-2]);
         if(i!=2){ROCDiffBuffer[i]=ROCBuffer[i]-ROCBuffer[i-1];}
         else{ROCDiffBuffer[i]=EMPTY_VALUE;}
      }
      else{ROCBuffer[i]=EMPTY_VALUE;}
      //Print("SMA[",i,"] = ",SMABuffer[i]," ROC[",i,"] = ",ROCBuffer[i]);
      //---
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
