#property copyright "JWiens"
#property strict

class PinBar {
   public:
      bool wasPinBar, wasBullish, wasBearish;
      PinBar(){
         check();
      }
      void check(){
         double o = Open[1],h = High[1],l = Low[1],c = Close[1];
         double spread = h-l, body = o-c;
         if(body<0){
            double nose = h-c, tail = o-l;
            if(((-1)*body<.3*spread)&&(tail>2*nose)){
               wasPinBar = true;
               wasBullish = true;
               wasBearish = false;
            }
            else{
               wasPinBar = false;
               wasBullish = false;
               wasBearish = false;
            }
         }
         if(body>0){
            double nose = c-l, tail = h-o;
            if((body<.3*spread)&&(tail>2*nose)){
               wasPinBar = true;
               wasBullish = false;
               wasBearish = true;
            }
            else{
               wasPinBar = false;
               wasBullish = false;
               wasBearish = false;
            }
         }
         
      }
      
}