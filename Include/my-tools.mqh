#property copyright "JWiens"
#property strict

/*bool isNewBar(){
   vNew = Volume[0];
   if(vOld > vNew){s
   vOld = vNew;
   return true;s
   }
   else{
   vOld = vNew;
   return false;
   }
}*/

void closeOrders(const int type){
   for(int i=0;i<OrdersTotal();i++){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false){break;}
      if(OrderSymbol() != Symbol()){continue;}
      if(OrderType() == type){
         if(type == OP_BUY){
            if(!OrderClose(OrderTicket(),OrderLots(),Bid,3,White)){
               Print("OrderClose error ",GetLastError());
            }
         }
         else{
            if(!OrderClose(OrderTicket(),OrderLots(),Ask,3,White)){
               Print("OrderClose error ",GetLastError());
            }
         }
         
      }
   } 
}
bool ordersOpen(){
   bool toggle;
   for(int i=0;i < OrdersTotal();i++){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false){
         toggle = false;
         break;
      }
      else if(OrderSymbol() == Symbol()){
         toggle = true;
         break;}
      else{
      toggle = false;
      break;
      }
   }
   if(toggle){return true;}
   else{return false;}
}

bool trendUp(int tf,int bigPeriod, int smallPeriod){
   double smaBig, smaSmall;
   smaBig = iMA(Symbol(),bigPeriod,100,0,MODE_SMA,PRICE_CLOSE,0);
   smaSmall = iMA(Symbol(),tf,smallPeriod,0,MODE_SMA,PRICE_CLOSE,0);
   if(smaSmall>smaBig){return true;}
   else{return false;}
}
bool trendDown(int tf, int bigPeriod, int smallPeriod){
   double smaBig, smaSmall;
   smaBig = iMA(Symbol(),tf,bigPeriod,0,MODE_SMA,PRICE_CLOSE,0);
   smaSmall = iMA(Symbol(),tf,smallPeriod,0,MODE_SMA,PRICE_CLOSE,0);
   if(smaSmall<smaBig){return true;}
   else{return false;}
}