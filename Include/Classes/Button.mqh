#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property strict

class Button{
   private:
      string BID;
   public:
      bool isPressed;
      Button(const long              chart_ID=0,               // chart's ID
                  const string            name="Button",            // button name
                  const int               sub_window=0,             // subwindow index
                  const int               x=300,                      // X coordinate
                  const int               y=0,                      // Y coordinate
                  const int               width=100,                 // button width
                  const int               height=28,                // button height
                  const ENUM_BASE_CORNER  corner=CORNER_RIGHT_UPPER, // chart corner for anchoring
                  const string            text="Button",            // text
                  const string            font="Arial",             // font
                  const int               font_size=16,             // font size
                  const color             clr=clrBlack,             // text color
                  const color             back_clr=C'236,233,216',  // background color
                  const color             border_clr=clrNONE,       // border color
                  const bool              state=false,              // pressed/released
                  const bool              back=false,               // in the background
                  const bool              selection=false,          // highlight to move
                  const bool              hidden=true,              // hidden in the object list
                  const long              z_order=0)                // priority for mouse click
  {
   isPressed = false;
//--- reset the error value
   ResetLastError();
//--- create the button
   ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0);
     BID = name;
//--- set button coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- set button size
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
//--- set the chart's corner, relative to which point coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- set text color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- set background color
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
//--- set border color
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- set button state
   ObjectSetInteger(chart_ID,name,OBJPROP_STATE,state);
//--- enable (true) or disable (false) the mode of moving the button by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- successful execution
  }
  void move(const int dx,const int dy);
  void resize(const int w, const int h);
  void text(const string txt);
  void bkgClr(const color clr);
  void setState(bool st8);
  int getState();

};

void Button::move(const int dx,const int dy){
   ObjectSetInteger(0,BID,OBJPROP_XDISTANCE,dx);
   ObjectSetInteger(0,BID,OBJPROP_YDISTANCE,dy);
}
void Button::resize(const int w, const int h){
   ObjectSetInteger(0,BID,OBJPROP_XSIZE,w);
   ObjectSetInteger(0,BID,OBJPROP_YSIZE,h);
}
void Button::text(const string txt){
   //Print("text method called");
   Print(txt);
   ObjectSetString(0,BID,OBJPROP_TEXT,txt);
}

void Button::bkgClr(const color clr){
   ObjectSetInteger(0,BID,OBJPROP_BGCOLOR,clr);
}
void Button::setState(bool st8){
   ObjectSetInteger(0,BID,OBJPROP_STATE,st8);
}
int Button::getState(){
   return(ObjectGetInteger(0,BID,OBJPROP_STATE));
}