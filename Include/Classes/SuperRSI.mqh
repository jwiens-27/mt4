#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property strict

class SuperRSI {
    private:
        double _obThresh, _osThresh;
    public:
        bool OB, OS;
        double RSI(int tf);
        void Calculate();
        void SuperRSI(double obThresh = 70, double osThresh = 30){
            _obThresh = obThresh;
            _osThresh = osThresh;
        }
}
double SuperRSI::RSI(int tf){
    return iRSI(Symbol(),tf,14,PRICE_CLOSE,0);
}
void SuperRSI::Calculate(){
    int tfs[] = {1,5,15,60,240}, numTfs = ArraySize(tfs);
    for(int i=0;i<numTfs;i++){
        if(RSI(tfs[i]>_obThresh){OB = true;}
        else if(RSI(tfs[i]<_osThresh){OS = true;}
        else{
            OB = false;
            OS = false;
            break;
        }
    }
}