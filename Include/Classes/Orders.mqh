#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property strict

class Orders {
    public:
        int sellOrdersOpen, buyOrdersOpen;
        double _lots, _TP, _SL;
        void SetTradeParams(double lots, double TP, double SL);
        int Buy();
        int Sell();
        void CloseAll(int type);
        bool CheckOpenOrders();
        void ShiftStops(int ticket, double shift);
        Orders(void){
            sellOrdersOpen = 0;
            buyOrdersOpen = 0;
        }
};
void Orders::SetTradeParams(double lots, double TP, double SL){
    _lots = lots;
    _TP = TP;
    _SL = SL;
}
int Orders::Buy(){
    int ticket;
    ticket = OrderSend(Symbol(),OP_BUY,_lots,Ask,5,Bid-_SL*10*Point,Bid+_TP*10*Point);
    if(ticket != -1){buyOrdersOpen++;}
    return ticket;
}
int Orders::Sell(){
    int ticket;
    ticket = OrderSend(Symbol(),OP_SELL,_lots,Bid,5,Ask+_SL*10*Point,Ask-_TP*10*Point);
    if(ticket != -1){sellOrdersOpen++;}
    return ticket;
}
void Orders::CloseAll(int type){
    for(int i=0;i<OrdersTotal();i++){
        if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false){break;}
        if(OrderSymbol() != Symbol()){continue;}
        if(OrderType() == type){
            if(type == OP_BUY){
                if(!OrderClose(OrderTicket(),OrderLots(),Bid,3,White)){
                Print("OrderClose error ",GetLastError());
                }
                else{buyOrdersOpen--;}
            }
            else{
                if(!OrderClose(OrderTicket(),OrderLots(),Ask,3,White)){
                Print("OrderClose error ",GetLastError());
                }
                else{sellOrdersOpen--;}
            }
        }
    }
}
bool Orders::CheckOpenOrders(){
    buyOrdersOpen = 0;
    sellOrdersOpen = 0;
    for(int i=0;i<OrdersTotal();i++){
        if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false){
            return false; 
        }
        if(OrderSymbol() != Symbol()){continue;}
        else if(OrderType() == OP_BUY){buyOrdersOpen++;}
        else if(OrderType() == OP_SELL){sellOrdersOpen++;}
    }
    if(buyOrdersOpen > 0 || sellOrdersOpen > 0){return true;}
    else{return false;}
}