#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property strict
class Trender {
        private:
            int _tf, _rsiPeriod, _adxPeriod;
        public:
            bool trendUp, trendDown;
            double trendStrength;
            double RSI(int shift);
            double ADX(int shift);
            void SetRSIPeriod(int period);
            void SetADXPeriod(int period);
            void Calculate(int shift);
            Trender(int TF = 0, int rsiPeriod = 14, int adxPeriod = 14){
                _tf = TF;
                _rsiPeriod = rsiPeriod;
                _adxPeriod = adxPeriod;
                Calculate();
            }
};
double Trender::RSI(int shift = 0){
    return iRSI(Symbol(),_tf,_rsiPeriod,PRICE_CLOSE,shift);
}
double Trender::ADX(int shift = 0){
    return iADX(Symbol(),_tf,_adxPeriod,PRICE_CLOSE,MODE_MAIN,shift);
}
void Trender::SetRSIPeriod(int period){
    _rsiPeriod = period;
}
void Trender::SetADXPeriod(int period){
    _adxPeriod = period;
}
void Trender::Calculate(int shift = 0){
    trendStrength = ADX(shift);
    if(RSI(shift)>60){
        trendUp = true;
        trendDown = false;
    }
    else if(RSI(shift)<40){
        trendDown = true;
        trendUp = false;
    }
    else{
        trendUp = false;
        trendDown = false;
        trendStrength = 0;
    }
}
