//+------------------------------------------------------------------+
//|                                                my-processors.mqh |
//|                                     Copyright 2019, Joshua Wiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
/*This is a collection of processor objects
used to identify market phases,
entry/exit points, candlestick patterns, 
and other notable market movements*/
#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property strict
#define UP 1
#define DOWN -1
#define RANGING 0
class Phaser {
    private:
        int shift;
        double RSI, ADX;
        double _GetRSI(int tf);
        double _GetADX(int tf);
        void _GaugeTrend();
        void _CheckTrend(int tf); 
    public:
        bool isTrending, isRanging; 
        int trendStrength, trendDirection;
        void GetTrend(int tf, int s);
        Phaser(int tf = 0){
            GetTrend(tf);
        }
};
void Phaser::GetTrend(int tf = 0, int s = 0){
    shift = s;
    trendStrength = 0;
    _CheckTrend(tf);
    if(isTrending){
        _GaugeTrend();
    }
}
double Phaser::_GetRSI(int tf){
    return iRSI(Symbol(),tf,14,PRICE_CLOSE,shift);
}
double Phaser::_GetADX(int tf){
    return iADX(Symbol(),tf,14,PRICE_CLOSE,MODE_MAIN,shift);//MODE_MAIN for strength w/o direction
}
void Phaser::_GaugeTrend(){
    int tfArray[] = {PERIOD_H1,PERIOD_H4,PERIOD_D1};
    double tRSI, tADX;
    int TD, oldTD = RANGING;
    for(int i=0;i<3;i++){
        tRSI = _GetRSI(tfArray[i]);
        tADX = _GetADX(tfArray[i]);
        if(i == 0 && tRSI>60){trendDirection = UP;}
        else if(i == 0 && tRSI<40){trendDirection = DOWN;}
        if(tRSI>60){ TD = UP;}
        else if(tRSI<40){ TD = DOWN;}
        else{TD = RANGING;}
        if((TD == oldTD && TD != RANGING) && tADX>25){trendStrength++;}
        oldTD = TD;
    }
}
void Phaser::_CheckTrend(int tf){
    ADX = _GetADX(tf);
    RSI = _GetRSI(tf);
    if((RSI>40 && RSI<60) && ADX<25){
        isRanging = true;
        isTrending = false;
        trendStrength = 0;
        trendDirection = RANGING;
    }
    else{
        isRanging = false;
        isTrending = true;
    }
}
class Orders {
    public:
        int sellOrdersOpen, buyOrdersOpen;
        double _lots, _TP, _SL;
        void SetTradeParams(double lots, double TP, double SL);
        void Buy();
        void Sell();
        void CloseAll(int type);
        Orders(void){
            sellOrdersOpen = 0;
            buyOrdersOpen = 0;
        }
};
void Orders::SetTradeParams(double lots, double TP, double SL){
    _lots = lots;
    _TP = TP;
    _SL = SL;
}
void Orders::Buy(){
    int ticket;
    ticket = OrderSend(Symbol(),OP_BUY,_lots,Ask,5,Bid-_SL*10*Point,Bid+_TP*10*Point);
    if(ticket != -1){buyOrdersOpen++;}
}
void Orders::Sell(){
    int ticket;
    ticket = OrderSend(Symbol(),OP_SELL,_lots,Bid,5,Ask+_SL*Point,Ask-_TP*Point);
    if(ticket != -1){sellOrdersOpen++;}
}
void Orders::CloseAll(int type){
    for(int i=0;i<OrdersTotal();i++){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false){break;}
      if(OrderSymbol() != Symbol()){continue;}
      if(OrderType() == type){
        if(type == OP_BUY){
            if(!OrderClose(OrderTicket(),OrderLots(),Bid,3,White)){
               Print("OrderClose error ",GetLastError());
            }
            else{buyOrdersOpen--;}
        }
        else{
            if(!OrderClose(OrderTicket(),OrderLots(),Ask,3,White)){
               Print("OrderClose error ",GetLastError());
            }
            else{sellOrdersOpen--;}
        }
      }
   }
}