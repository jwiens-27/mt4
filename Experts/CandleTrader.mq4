//+------------------------------------------------------------------+
//|                                                    CandleTrader.mq4 |
//|                                                           JWiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "JWiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Classes/Orders.mqh>
#include <Classes/Trender.mqh>
//#include <CandleSticks.mqh>

input int   tp = 50;
input int   sl = 25;
input int   spikeVThresh = 20; 
input int   spikePThresh = 10;

Orders or;
Trender tr;
//PinBar bar;
double pivot, price;
bool buyCocked = false, sellCocked = false;

int OnInit(){
    return(INIT_SUCCEEDED);
}
void OnDeinit(const int reason){}

void OnTick(){
    or.SetTradeParams(AccountBalance()/10000,tp,sl);
    or.CheckOpenOrders();
    //bar.check();
    tr.Calculate();
    //if(tr.trendUp && ThreeLineStrikeUp() && Volume[0]<=1){or.Buy();}
    //if(tr.trendDown && ThreeLineStrikeDown() && Volume[0]<=1){or.Sell();}
    //if(bar.wasPinBar && bar.wasBearish && Volume[0]<=1){or.Sell();}
    //if(bar.wasPinBar && bar.wasBullish && Volume[0]<=1){or.Buy();}
    if(SpikeUp() && Volume[0] <= 1){or.Buy();}


}
bool ThreeLineStrikeUp(){
    if(Close[2]<Low[3]&&Low[3]<Low[4]&&Close[1]>Open[4]){return true;}
    else{return false;}
}
bool ThreeLineStrikeDown(){
    if(Close[2]>High[3]&&High[3]>High[4]&&Close[1]<Open[4]){return true;}
    else{return false;}
}
bool SpikeUp(){
    if(Volume[1] > spikeVThresh && (Close[1]-Open[1])*10000 > spikePThresh){return true;}
    else{return false;}
}