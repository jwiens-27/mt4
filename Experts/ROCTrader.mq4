//+------------------------------------------------------------------+
//|                                                    ROCTrader.mq4 |
//|                                                           JWiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "JWiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <my-tools.mqh>
#include <Classes/Phaser.mqh>
#include <Classes/Manager.mqh>

//--- input parameters
input double   diffThresh=0.5, slopeThresh = .015;
input int      periods=5,SL = 100, TP = 1000, tBP = 50, tSP = 20, OB = 70, OS = 30;
Manager man;

//--- global variables
int ticket;
double roc[], rocDiff[];
bool sellCocked = false, buyCocked = false;
Phaser p1;
//bool bPosOpen = false, sPosOpen = false;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   ArrayResize(roc,3);
   ArrayResize(rocDiff,3); 
   ArrayInitialize(roc,EMPTY_VALUE);
   ArrayInitialize(rocDiff,EMPTY_VALUE);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   p1.GetTrend();
   for(int i=0;i<3;i++){
      roc[i]=iCustom(Symbol(),0,"iROC",periods,0,i);
      rocDiff[i]=iCustom(Symbol(),0,"iROC",periods,1,i);
   }

   if( ( (roc[1]<0 && roc[2]>0) || sellCocked) && Volume[0]<=1){
      sellCocked = true;
      buyCocked = false;
      
      if((roc[1]<=-slopeThresh)){
         ticket = OrderSend(Symbol(),OP_SELL,AccountBalance()/10000,Bid,5,Ask+SL*Point,Ask-TP*Point);
         closeOrders(OP_BUY);
         sellCocked = false;
      }
   }
   if(((roc[1]>0 && roc[2]<0) || buyCocked) && Volume[0]<=1){
      buyCocked = true;
      sellCocked = false;
      
      if(roc[1]>=slopeThresh){
         ticket = OrderSend(Symbol(),OP_BUY,AccountBalance()/10000,Ask,5,Bid-SL*Point,Bid+TP*Point);
         closeOrders(OP_SELL);
         buyCocked = false;
      }
   }
   if(rocDiff[0]<-diffThresh){
      closeOrders(OP_BUY);
   }
   if((rocDiff[0]>diffThresh)){
      closeOrders(OP_SELL);
   }

}


//+------------------------------------------------------------------+
