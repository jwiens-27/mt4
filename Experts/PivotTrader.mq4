//+------------------------------------------------------------------+
//|                                                    PivotTrader.mq4 |
//|                                                           JWiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "JWiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Classes/Orders.mqh>
#include <Classes/Trender.mqh>

input ENUM_TIMEFRAMES tf = PERIOD_W1;
input int tp = 50, sl = 25;

Orders o;
Trender tr;
double pivot, price;
bool buyCocked = false, sellCocked = false;

int OnInit(){
    return(INIT_SUCCEEDED);
}
void OnDeinit(const int reason){}

void OnTick(){
    o.SetTradeParams(AccountBalance()/10000,tp,sl);
    o.CheckOpenOrders();
    tr.Calculate();
    if(((tr.trendUp || tr.trendDown) && tr.trendStrength > 20) && (TimeHour(TimeCurrent())>=7 && TimeHour(TimeCurrent())<=14)){
        for(int i=0;i<9;i++){
            pivot = iCustom(Symbol(),0,"All Pivot Points",tf,i,0);
            if(i==0){Print(pivot);}
            if(Open[0]>pivot && Close[0]<pivot && !sellCocked && o.buyOrdersOpen == 0){
                //Print("Buy Cocked");  
                o.CloseAll(OP_SELL); 
                Print("S = ",o.sellOrdersOpen); 
                buyCocked = true;
            }
            if(Open[0]<pivot && Close[0]>pivot && !buyCocked && o.sellOrdersOpen == 0){
                //Print("Sell Cocked");
                o.CloseAll(OP_BUY);
                Print("B = ",o.buyOrdersOpen); 
                sellCocked = true; 
                }
            if(((Open[0]<pivot && Close[0]>pivot && Volume[0]<=1)||(Low[0]<pivot && Close[0]>pivot)) && buyCocked){
                if(o.Buy() != -1){buyCocked = false;}
            }
            if(((Open[0]>pivot && Close[0]<pivot && Volume[0]<=1)||(High[0]>pivot && Close[0]<pivot)) && sellCocked){
                if(o.Sell() != -1){sellCocked = false;}    
            }
        }
    }
}
