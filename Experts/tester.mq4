//+------------------------------------------------------------------+
//|                                                       tester.mq4 |
//|                                     Copyright 2019, Joshua Wiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, Joshua Wiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Classes/Button.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int tog = 0;
Button pp();
int OnInit(){

 
   ChartRedraw();

   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   //if(pp.getState != tog){
      Print(pp.getState());
      //tog = pp.getState();
   //}
   
}
//+------------------------------------------------------------------+
