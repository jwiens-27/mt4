//+------------------------------------------------------------------+
//|                                               IchimokuTrader.mq4 |
//|                                                     Joshua Wiens |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Joshua Wiens"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Classes/Orders.mqh>

input double TP, SL;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
Orders o;
double TK[3], KJ[3], SSA[3], SSB[3];


int OnInit(){
   ArrayInitialize(TK,EMPTY_VALUE);
   ArrayInitialize(KJ,EMPTY_VALUE);
   ArrayInitialize(SSA,EMPTY_VALUE);
   ArrayInitialize(SSB,EMPTY_VALUE);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   o.SetTradeParams(AccountBalance()/10000,TP,SL);
   for(int i=0;i<3;i++){
      TK[i] = iIchimoku(NULL,0,9,26,52,MODE_TENKANSEN,i);
      KJ[i] = iIchimoku(NULL,0,9,26,52,MODE_KIJUNSEN,i);
      SSA[i] = iIchimoku(NULL,0,9,26,52,MODE_SENKOUSPANA,i);
      SSB[i] = iIchimoku(NULL,0,9,26,52,MODE_SENKOUSPANB,i);
   }
   if((TK[0]>KJ[0]&&TK[1]<KJ[1])&&Volume[0]<=1){
      o.CloseAll(OP_SELL);
      if(TK[0]<SSB[0]&&TK[0]<SSA[0]){
         o.Buy();
      }
   }
   if((TK[0]<KJ[0]&&TK[1]>KJ[1])&&Volume[0]<=1){
      o.CloseAll(OP_BUY);
      if(TK[0]>SSB[0]&&TK[0]>SSB[0]){
         o.Sell();
      }
   }
   
   
   
}
//+------------------------------------------------------------------+
