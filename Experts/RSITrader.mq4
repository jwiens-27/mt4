//+------------------------------------------------------------------+
//|                                                    RSITrader.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//--- input parameters
input int      periods = 14;
input int      obLevel = 70;
input int      osLevel = 30;
input int      TP = 1000;
input int      SL = 250;
input int      TF;
//--- Global Variables
int ticket;
double rsi[], lots;
bool bPosOpen = false, sPosOpen = false;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   ArrayResize(rsi,3);
   ArrayInitialize(rsi,EMPTY_VALUE);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   for(int i=0;i<3;i++){
      rsi[i]=iRSI(NULL,TF,periods,0,i);
   }
   if(OrderSelect(ticket,SELECT_BY_TICKET)==false){
      sPosOpen = false;
      bPosOpen = false;
   }
   
   
   if(rsi[1]<obLevel&&rsi[2]>obLevel&&!sPosOpen){
      if(bPosOpen){
         if(OrderClose(OrderTicket(),OrderLots(),Bid,5)){
            bPosOpen = false;
            Print("if bPosOpen failed");
         }  
      }
      ticket = OrderSend(Symbol(),OP_SELL,AccountBalance()/10000,Bid,5,Ask+SL*Point,Ask-TP*Point);
      if(ticket!= -1){
      sPosOpen = true;
      }
   }
   if(rsi[1]>osLevel&&rsi[2]<osLevel&&!bPosOpen){
      if(sPosOpen){
         if(OrderClose(OrderTicket(),OrderLots(),Ask,5)){
            sPosOpen = false;
            Print("if sPosOpen failed");
         }
      }
      ticket = OrderSend(Symbol(),OP_BUY,AccountBalance()/10000,Ask,5,Bid-SL*Point,Bid+TP*Point);
      if(ticket!=-1){
         bPosOpen = true;
      }
   }
   
   if(bPosOpen&&rsi[1]>50){
      if(OrderClose(OrderTicket(),OrderLots(),Bid,5)){
         bPosOpen = false;
         Print("if bPosOpen2 failed");
      }
   }
   if(sPosOpen&&rsi[1]<50){
      if(OrderClose(OrderTicket(),OrderLots(),Ask,5)){
         sPosOpen = false;
         Print("if sPosOpen2 failed");
      }
   }
      
   
   
   
   
  }
//+------------------------------------------------------------------+
